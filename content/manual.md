---
title: Manual
layout: page
---

Here is a quick overview what to do after you downloaded the latest beta image for the Raspberry Pi 4:

# Flashing

After downloading the img, use etcher or similar program to flash the img on to a micro SDCard of at least 8GB size: https://www.balena.io/etcher/.

After flashing, you may resize the available space on the microsdcard by using kpartitionmanager or gparted if you plan to extensively install more software on it.

# Power up

Otherwise simply put the microsd into the device and power it up.
You should see a rainbow image shortly flashing up, then loading the operating system and showing the Plasma Bigscreen Welcome screen.

Once you see the homescreen, connect to the internet either by ethernet cable or using wifi connection.
To establish a wifi connection, navigate the active selection cursor either to the Settings row and select “Wireless” tile, or move the cursor up to the “systray” panel and click on the wifi symbol there.

Pick a wireless connection and enter the password via the virtual keyboard.
Once a connection is established, Mycroft tries to connect to its home server to function as central hub for the voice control.
The default configuation uses Mycrofts server, so it requires to register the device for interaction and therefore an account.

Note it is possible to set up your own server and connect your Bigscreen box to that instead of Mycrofts server, plus your own STT engine like Mozillas DeepSpeech.
For DeepSpeech STT to work, you need quite a powerful machine though.
Here is some documentation about how to make Myroft use a different speech engine, hosted on your own server: https://mycroft-ai.gitbook.io/docs/using-mycroft-ai/customizations/stt-engine

# Mycroft Server Pairing

When you first connect your device to the internet, this screen will pop up automatically.
Please wait a minute or two before you get shown a code like here:

![MyCroft pairing code](/img/pages/manual/mycroft-registration-dialog.jpg)

To use MyCroft voice control, you need to pair your device on home.mycroft.ai.

> You can do this from any PC or smartphone, the pairing does not need to be done from the device itself.
> When you have an account and are logged in, go to https://account.mycroft.ai/devices to register your device once.

![MyCroft: add device](/img/pages/manual/mycroft-configuration-01-add-device.png)

> Enter the pairing code and give the device a name and location:

![MyCroft: device configuration](/img/pages/manual/mycroft-configuration-02-device-configuration.png)

> Make sure all fields are actually filled out and voice type selected to be able to click NEXT:

![MyCroft: preferences](/img/pages/manual/mycroft-configuration-03-preferences.png)

> Here is a complete guide for pairing a new device: https://mycroft-ai.gitbook.io/docs/using-mycroft-ai/pairing-your-device 

# Use Bigscreen

Press the “HOME” Screen on your remote to exit the registration and start using Bigscreen.

You only have to do this once.
When you restart Bigscreen, it should automatically reconnect to your wifi first, then even when mycroft mentiones that it is not connected, it will reconnect after a short time once internet wifi is connected.

Once you successfully registered the device, you can start using the Voice Apps normally.

Try e.g. saying “Hey Mycroft: Soundcloud Rihanna”.

# Notes

The credentials for the KDE Neon reference image are:

- User: mycroft
- Password: mycroft

As a new Bigscreen user, go to Apps and Skills to look for new ways to expand your box with new skills and functionality added by developers from all over the world.
And now: Have fun! 🙂
