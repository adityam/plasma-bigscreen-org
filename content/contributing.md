---
title: Contributing
menu:
  main:
    parent: community
    weight: 1
---

This article describes how you can contribute to Plasma Bigscreen, take part in its design and development.
This page addresses a wider range of skills, each paragraph explains how to get you going from a different
starting point.
Where else do you have the opportunity to shape a complete TV software environment?

Consider joining the Plasma Bigscreen [Matrix channel](https://matrix.to/#/#plasma-bigscreen:kde.org) in order to get in touch with other developers.

# I found a bug

Reporting bugs is an helpful way to help making Plasma Bigscreen better without the need to know how to code.
You can report bugs for Plasma Bigscreen applications in their respective [invent project](https://invent.kde.org/).

Some base components like Plasma or KWin are using [bugs.kde.org](https://bugs.kde.org) instead.
Please report bugs there.

# I am a designer

For designers, the Visual Design Group [Matrix channel](https://matrix.to/#/#kde-vdg:kde.org) is a great place to start discussions about designs, and getting in touch with other designers.
You can pick up ideas from there, and post feedback or even your own designs to get input from others.

# I want to write an app

There are multiple ways to bring your application to Plasma Bigscreen.

Consider checking out the [Kirigami documentation](https://develop.kde.org/frameworks/kirigami/).

-   Write a native app: You can bring a new app that is already using KDE Frameworks to Plasma Bigscreen.
    Make the UI touch-friendly, and talk to us how it can best be integrated.

-   Port an existing app: Plasma Bigscreen is an inclusive system, allowing to run different apps.
    If your application is ready for the use on touchscreens and high-dpi displays, it's usually possible to install it using the package manager.
    A first step is to try it and fix possible issues.

-   Android apps: Support for Android apps is currently work-in-progress.
    Talk to us about its status.

Ultimately, you will need it to be packaged for the relevant distributions that offer Plasma Bigscreen (ex. [postmarketOS](https://postmarketos.org/)).

# I want to work on the Plasma Bigscreen system

If you want develop on the Plasma Bigscreen “core” system or functionality, join the [Matrix channel](https://matrix.to/#/#plasma-bigscreen:kde.org) to discuss it with other developers.
If you're up for picking up any other task, let us know and we'll get you going.

# I want to make Plasma Bigscreen available on my Linux distribution

We love your effort to bring Plasma Bigscreen to a wider audience, and we're ready to help you with that.
If you would like to offer Plasma Bigscreen on a not-yet supported Linux distribution, the following information is useful:

-   The source code is hosted on [Gitlab](https://invent.kde.org)

-   KDE provides a reference image based on KDE Neon, you can use this to test against, look at integration-level solutions and borrow its tricks to get Plasma Bigscreen running

-   If you want to improve things in an existing system, get in touch with us via the Plasma mailing list, or talk directly to the people working on the distro and packaging.

Do let us know of your efforts on the [Plasma mailing list](https://mail.kde.org/mailman/listinfo/plasma-devel), so we can help you to get going and provide advice that may save yourself time and headaches.

# I have this great idea for a new feature...

...but I can't do it all by myself.
A great way to find like-minded people that may be able to help you make your idea a reality is to post
it to the [Matrix channel](https://matrix.to/#/#plasma-bigscreen:kde.org) to gather feedback on it.
Maybe someone else has a similar goal, or you find people who want to help you.

#I just want to help, throw a task at me!

Great!
We always need help.
In order to find something that you find fun and rewarding to work on, a good first step is to find out which itch you have with Plasma Bigscreen, and how it can be scratched.
What's nagging you? Now give us a shout-out, best via the [Plasma mailing list](https://mail.kde.org/mailman/listinfo/plasma-devel).
You can also make yourself known in the [Matrix channel](https://matrix.to/#/#plasma-bigscreen:kde.org).
There's plenty to do, tasks for every skill and level, and you'll find it's fun to work on and learn from each other.

Consider also checking out open issues for Plasma Bigscreen projects on [Gitlab](https://invent.kde.org/).
