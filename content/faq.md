---
title: Frequently Asked Questions
layout: page
menu:
  main:
    parent: project
    weight: 5
    name: FAQ
---


## Help! My TV remote does not work, how do I get my TV remote working?

Many TV manufacturers require HDMI-CEC mode to be enabled manually, one needs to refer to the TV user guide to figure out how to configure the HDMI-CEC option on their television set.
The HDMI-CEC option can be found under various names depending on the TV manufacturers, some for example: (TCL TV: T-Link, Panasonic TV: Viera Link, Samsung TV: Anynet+, Sony TV: Bravia Sync).

Also make sure your Plasma Bigscreen device is actually supported by libcec, [check their supported hardware](https://github.com/Pulse-Eight/libcec#supported-hardware).

## Some buttons on my TV remote are not working and I can’t exit an application or use the back button properly. How do I fix it?

HDMI-CEC on the beta image is in a testing phase.
We have only been able to test the functionality on a few selected range of TV sets and have mapped TV remotes on the basis of those working devices.
You can map and test your TV remote following a few simple steps of debugging and editing files listed below.

_Test if the KEY is working with HDMI-CEC and extract it’s KEY CODE:_

```sh
cd ~/.config./autostart-scripts/
python3 cec-daemon.py
```

Once the script it running, press the button on your TV remote to extract its KEY CODE, if no KEY CODE is found the KEY might not be part of HDMI-CEC controls enabled on your TV set, refer to the TV User Guide to know which keys are enabled under your TV manufacturers HDMI-CEC implementation.

_Adding the found KEY CODE and mapping it in the CEC daemon:_

```sh
cd ~\.config\autostart-scripts\
nano cec-daemon.py
```

- Locate KEYMAP = {} in the daemon script
- Add your KEY CODE in the following format to the list: “9: u.KEY\_HOMEPAGE”
- In the below example “u.KEY\_HOMEPAGE” is mapped to the home button that is used to exit an application
- “9” being the Key Code
- “u.KEY\_HOMEPAGE” being the action the key should perform

## I have a generic USB remote but it’s missing the home key, how do I exit applications?

Not all generic USB remotes are built alike, therefore we recommend using a tested product like the “Wechip G20 Air Mouse With Microphone Remote”.
If in-case you are unable to get your hands on one, you can still map an existing key on the remote.

_Mapping the window close button to a button on a USB remote:_

```sh
cd ~\.config\
nano kglobalshortcutsrc
```

- Find the entry “Window Close” located under `[Kwin]`
- Assign your button to the “Window Close” entry
- Example: `Window Close=Alt+F4\t’YourButtonHere’,Alt+F4\t’YourButtonHere’,Close Window`

## Voice applications do not start on boot and I see a blank screen. What should I do?

Voice applications are only accessible once MyCroft is ready and has started, you will be notified on the top panel when MyCroft has started and is in a ready state.

## How do I exit applications using a external keyboard?

“Alt+F4” is the general shortcut assigned to closing applications using a external keyboard.
Custom keys can be assigned to the following file for various actions:

```sh
cd ~\.config\
nano kglobalshortcutsrc
```

## What is the hotkey assigned to activate MyCroft voice control without a USB mic remote?

There is no hotkey assigned to MyCroft, the Generic USB remotes that have a mic button only activate the mic input on the USB remote hardware, to use MyCroft one needs to activate it with the hotword “Hey MyCroft”.

To use MyCroft one does not require a USB mic enabled remote specifically, any microphone that can be connected to your device should work.

For issues and troubleshooting your microphone not working one can refer to: https://mycroft-ai.gitbook.io/docs/using-mycroft-ai/troubleshooting/audio-troubleshooting.

## How to contribute & upload your custom keymap for CEC

Currently the development repository of the CEC daemon can be found at https://invent.kde.org/adityam/easycec with instructions on how to add a device with custom keymap.

## Can Android apps work on Plasma Bigscreen?

There are projects out there like [Anbox](https://anbox.io/) which is Android running inside a Linux container, and use the Linux kernel to execute applications to achieve near-native performance.
This could be leveraged in the future to have Android apps running on top of a GNU/Linux system with the Plasma Bigscreen platform but it's a complicated task, and as of *today* (August 5th, 2021) some distributions already support Anbox and you can run Plasma Bigscreen on top of those distributions.

## Can I run Plasma Bigscreen on my TV or setup-box?

Currently, Plasma Bigscreen runs on the following device types:

* **(Recommended) Raspberry Pi 4:** We offer official images built for the Raspberry Pi 4 on top of KDE Neon.

* **postmarketOS devices:** postmarketOS is a distribution based on Alpine Linux that can be installed on Android smartphones but also on SBC's like the Raspberry Pi 4 and other devices.
  Please find your device from the [list of supported devices](https://wiki.postmarketos.org/wiki/Devices) and see what's working, then you can follow the [pmOS installation guide](https://wiki.postmarketos.org/wiki/Installation_guide) to install it on your device.
  Your mileage may vary depending on the device in use and when you use a device in the testing category it is **not** necessarily representative of the current state of Plasma Bigscreen.

## I've installed Plasma Bigscreen, what is the login password?

If you've installed the reference KDE Neon image for the Raspberry Pi 4, the password and username are both "mycroft" and you can then change the password afterwards by running "passwd" from a tty.

## What's the state of the project?

Plasma Bigscreen is currently under heavy development and is not intended to be used as a daily driver.
