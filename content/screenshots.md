---
title: Screenshots
layout: page
screenshots:
  - url: /img/pages/screenshots/homescreen.png
    name: "Plasma Bigscreen homescreen"
menu:
  main:
    parent: project
    weight: 2
sassFiles:
  - scss/screenshots.scss
  - scss/components/swiper.scss
jsFiles:
- https://cdn.kde.org/aether-devel/version/kde-org/applications.3e16ae06.js
---

{{< screenshots name="screenshots" >}}
