---
title: Distributions offering Plasma Bigscreen
menu:
  main:
    weight: 4
    name: Install
sassFiles:
  - scss/get.scss
---

## Neon based reference rootfs

![](/img/neon.svg)

Image based on KDE Neon.
KDE Neon itself is based upon Ubuntu 20.04 (focal).
This image is based on the dev-unstable branch of KDE Neon, and always ships the latest versions of KDE frameworks, KWin and Plasma Bigscreen compiled from git master.

##### Download:

* [Raspberry Pi 4](https://sourceforge.net/projects/bigscreen/files/rpi4/beta)

### Installation

Download the image, uncompress it and flash it to a SD-card using `dd` or a graphical tool.
The Raspberry Pi will automatically boot from a SD-Card.

For a complete step-by-step readme, [click here](/manual).

## postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) is a pre-configured Alpine Linux that can be installed on smartphones, SBC's and other mobile devices. View the [device list](https://wiki.postmarketos.org/wiki/Devices) to see the progress for supporting your device.

For devices that do not have prebuilt images, you will need to flash it manually using the `pmbootstrap` utility.
Follow instructions [here](https://wiki.postmarketos.org/wiki/Installation_guide).
Be sure to also check the device's wiki page for more information on what is working.

[Learn more](https://postmarketos.org)

##### Download:

* [Community Devices](https://postmarketos.org/download/)
